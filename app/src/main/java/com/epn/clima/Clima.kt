package com.epn.clima
import com.google.gson.annotations.SerializedName

data class ResponseClima(var clima: List<Clima>) {}

data class Clima(//para serializacion se usa un data class
    @SerializedName("main")
    var name: String?,
    var description: String?,
    var icon: String?
) {}
