package com.epn.clima


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class main_clima : Fragment() {


    lateinit var cityName: TextView
    lateinit var weatherName: TextView
    lateinit var searchButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_clima, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cityName = view.findViewById(R.id.city_name)
        weatherName = view.findViewById(R.id.weather_response)
        searchButton = view.findViewById(R.id.search_button)

        searchButton.setOnClickListener { getWeather() }
    }

    fun getWeather() {

        val weatherService = ServicioClima.instace

        var weatherCall = weatherService.getClima("2.5", cityName.text.toString())

        weatherCall.enqueue(object: Callback<ResponseClima> {
            override fun onFailure(call: Call<ResponseClima>, t: Throwable) {

            }

            override fun onResponse(call: Call<ResponseClima>, response: Response<ResponseClima>) {

                val weatherList = response.body() as ResponseClima
                val firstWeather = weatherList.clima?.get(0)

                weatherName.text = firstWeather?.name ?: "Error"

                //val iconUrl=
                  //      "http://openweathermap.org/img/w/${firstWeather?.icon?:"10d"}.png"
                //Glide.with(context!!)

            }

        })
    }


}
