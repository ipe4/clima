package com.epn.clima


import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ServicioClima {
    @GET("/data/{api_version}/weather")
    fun getClima(
        @Path("api_version") version: String = "2.5",
        @Query("q") city: String,
        @Query("appid") appIdd: String="fd231b7f7fee69a4eac9e6d0d27f4628"
    ): Call<ResponseClima>


    companion object {
        val instace: ServicioClima by lazy {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            retrofit.create<ServicioClima>(ServicioClima::class.java)
        }
    }
}


